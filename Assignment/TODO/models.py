from django.db import models

# Create your models here.

class ToDo(models.Model):
    TODO_STATE_CHOICE = (
        ('todo', 'Todo'),
        ('in-progress', 'In-Progress'),
        ('done', 'Done'),
    )
    todo = models.TextField(null=True,blank=True)
    due_date = models.DateField(null=True,blank=True)
    state = models.CharField (max_length=20, choices=TODO_STATE_CHOICE, default="todo")
    created_at = models.DateTimeField(auto_now=False, auto_now_add=True)

    def __str__(self):
        return self.todo

    class Meta:
        db_table = 'to_do'
