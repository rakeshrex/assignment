from django.contrib import admin
from .models import ToDo
# Register your models here.


class TODOAdmin(admin.ModelAdmin):
    list_display = ['todo', 'state', 'due_date']
    search_fields = ['todo']
    list_per_page = 100

admin.site.register(ToDo, TODOAdmin)


