from rest_framework import exceptions
from rest_framework import serializers
from TODO.models import ToDo


class TodoListSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    todo = serializers.CharField()
    due_date = serializers.DateField()
    state = serializers.CharField()

class TodoAddSerializer(serializers.Serializer):
    todo = serializers.CharField(required=True)
    due_date = serializers.DateField(required=True)
    state = serializers.CharField(required=True)

class TodoUpdateSerializer(serializers.Serializer):
    id = serializers.IntegerField(required=True)
    todo = serializers.CharField(required=True)
    due_date = serializers.DateField(required=True)
    state = serializers.CharField(required=True)