from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from TODO.models import *
from .serializers import TodoListSerializer, TodoAddSerializer, TodoUpdateSerializer
from django.http import *


##List all TODOs
class ToDoAPIView(APIView):

    def get(self, request, *args, **kwargs):
        data = request.data
        state = data[0]["state"]
        due_date = data[0]["due_date"]

## Validate the state filter key. return if wrong parameter
        if not (state == 'todo') | (state == 'in-progress') | (state == 'done'):
            return Response({"message": "Require correct state key", "data": {}, "status": status.HTTP_400_BAD_REQUEST},
                            status=status.HTTP_400_BAD_REQUEST)

## Filter logic start. Able to filter TODOs by state and/or due-date.
        if state:
            if due_date:
                queryset = ToDo.objects.filter(state=state, due_date=due_date )
            else:
                queryset = ToDo.objects.filter(state=state)
        elif due_date:
            queryset = ToDo.objects.filter(due_date=due_date)
        else:
            queryset = ToDo.objects.all()
## Filter logic end.

        serializer = TodoListSerializer(queryset, many=True)
        return Response({"message": "success", "status": status.HTTP_200_OK, "data": serializer.data }, status.HTTP_200_OK)


##Add one or more new TODOs
    def post(self, request, *args, **kwargs):
        data = request.data

##Validate the blank key. Return if wrong parameter
        for validate_key in data:
            serializer = TodoAddSerializer(data=validate_key)  ##searilize the valid type data
            if serializer.is_valid(raise_exception=True):
                todo = validate_key['todo']
                due_date = validate_key['due_date']
                state = validate_key['state']

##Validate todo choice field key. Return if wrong parameter
                if not (state == 'todo') | (state == 'in-progress') | (state == 'done'):
                    return Response({"message": "Provide valid state key", "data": {}, "status": status.HTTP_400_BAD_REQUEST},
                        status=status.HTTP_400_BAD_REQUEST)

##Logic for add validated record into model
        try:
            for store_key in data:
                ToDo.objects.create(todo=store_key['todo'], due_date=store_key['due_date'], state=store_key['state'])
        except:
            return Response({"message": "server error", "data": {}, "status": status.HTTP_500_INTERNAL_ERROR})

        return Response({"message": "successfully created", "data": {}, "status": status.HTTP_201_CREATED},
                        status=status.HTTP_201_CREATED)


##Update one or more TODOs
    def put(self, request, *args, **kwargs):
        data = request.data

##Validate the blank key. Return if wrong parameter
        for validate_key in data:
            serializer = TodoUpdateSerializer(data=validate_key)  ##Serialize the require field
            if serializer.is_valid(raise_exception=True):
                id = validate_key['id']
                todo = validate_key['todo']
                due_date = validate_key['due_date']
                state = validate_key['state']

                todo_update_obj = ToDo.objects.filter(id=id)
                if not todo_update_obj:
                    return Response({"message": "Invalid key in update request", "data": {}, "status": status.HTTP_400_BAD_REQUEST},
                        status=status.HTTP_400_BAD_REQUEST)

                if not (state == 'todo') | (state == 'in-progress') | (state == 'done'):
                    return Response({"message": "Provide valid state key", "data": {}, "status": status.HTTP_400_BAD_REQUEST},
                        status=status.HTTP_400_BAD_REQUEST)
##Validate logic end
##Update logic start
        try:
            for update_key in data:
                ToDo.objects.filter(id=update_key['id']).update(todo=update_key['todo'], due_date=update_key['due_date'], state=update_key['state'])
        except:
            return Response({"message": "server error", "data": {}, "status": status.HTTP_500_INTERNAL_ERROR})
##Update logic end

        return Response({"message": "successfully updated", "data": {}, "status": status.HTTP_201_CREATED},
                        status=status.HTTP_201_CREATED)

##Delete one or more TODOs.
    def delete(self, request, *args, **kwargs):
        delete_list = request.data

##Validate the key. Return if wrong key
        for validate_key in delete_list:
                todo_del_obj = ToDo.objects.filter(id=validate_key)
                if not todo_del_obj:
                    return Response({"message": "Invalid key in deletion request", "data": {}, "status": status.HTTP_400_BAD_REQUEST},
                                    status=status.HTTP_400_BAD_REQUEST)
##Validate logic end
##Delete the validated record, logic start
        for del_key in delete_list:
            todo_del_obj = ToDo.objects.filter(id=del_key)
            todo_del_obj.delete()
##Delete login end

        return Response({"message": "successfully deleted", "data": {}, "status": status.HTTP_200_OK},
                        status=status.HTTP_200_OK)
