from django.urls import path

from . import views

app_name = 'todo'

urlpatterns = [

    path('todo/', views.ToDoAPIView.as_view(), name='todo')
]


